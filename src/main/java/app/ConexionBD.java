package app;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// Patrón singleton clásico
// Implementación clásica. En este caso se emplea una técnica conocida como instanciación perezosa para crear el singleton; como resultado, 
// la instancia de singleton no se crea hasta que se llama al método getInstance() 
// por primera vez. Esta técnica garantiza que las instancias singleton se creen solo cuando sea necesario.
public class ConexionBD {
    private static final String JDBC_URL = "jdbc:mariadb://192.168.56.102:3306/empresa_ad";
    private static Connection con = null;        

    public static Connection getConexion() throws SQLException {
        if (con == null) {
            Properties pc = new Properties();
            pc.put("user", "batoi");
            pc.put("password", "1234");
            con = DriverManager.getConnection(JDBC_URL, pc);
        }
        return con;
    }

    public static void cerrar() throws SQLException {
        if (con != null) {
            con.close();
            con = null;
        }
    }

}

package app;


import java.sql.Connection;
import java.sql.SQLException;

public class App {

	public static void main(String[] args) throws SQLException {
		Connection con1 = ConexionBD.getConexion();		
		Connection con2 = ConexionBD.getConexion();
		System.out.println("Son la misma conexión: " + (con1==con2));
		
		Impresora impresora1 = Impresora.getInstance();
		Impresora impresora2 = Impresora.getInstance();
		System.out.println("Son el mismo recurso impresora: " +(impresora1==impresora2));
		impresora1.imprimir("Wilfredo colgó un wav wifi en la world wide web");
		impresora2.imprimir("¿Qué wav colgó con su wifi wilfredo en la world wide web?");	

	}

}


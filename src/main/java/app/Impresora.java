package app;

// Patrón singleton
// La implementación singleton más sencilla consiste en un constructor privado,
// un campo para contener una instancia y un método público de acceso estático con un nombre como getInstance(). 
public class Impresora {
	private String modelo = "HP LaserJet M402";

	private static Impresora impresora = new Impresora();

	private Impresora() {
	}

	public static Impresora getInstance() {
		return impresora;
	}

	public void imprimir(String texto) {
		System.out.println("Impresora " + modelo + "... imprimiendo: " + texto);
	}
}